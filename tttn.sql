-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 03 Avril 2015 à 11:50
-- Version du serveur :  5.6.21
-- Version de PHP :  5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `tttn`
--

-- --------------------------------------------------------

--
-- Structure de la table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
`id` bigint(20) NOT NULL,
  `address` text CHARACTER SET utf8 NOT NULL,
  `email` text NOT NULL,
  `name` text CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `sex` int(11) NOT NULL,
  `type` text CHARACTER SET utf8 NOT NULL,
  `username` text NOT NULL,
  `job` text CHARACTER SET utf8 NOT NULL,
  `company` text CHARACTER SET utf8 NOT NULL,
  `lastvisit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enable` int(11) NOT NULL DEFAULT '1',
  `birthdate` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `account`
--

INSERT INTO `account` (`id`, `address`, `email`, `name`, `password`, `telephone`, `sex`, `type`, `username`, `job`, `company`, `lastvisit`, `createddate`, `enable`, `birthdate`) VALUES
(42, 'Quận 11, HCM', 'tavsta47@gmail.com', 'Trần Phúc', '202cb962ac59075b964b07152d234b70', '01689 167 693', 0, '', 'phuc', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1992-10-04'),
(43, '', '', 'Trung', '202cb962ac59075b964b07152d234b70', '', 0, '', 'trung', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, ''),
(44, '', '', 'Trần Phúc', '40f3edbe92ab15daae1d82a84c82b3b4', '', 0, '', 'phuc12345', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '');

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
`id` bigint(20) NOT NULL,
  `globalid` bigint(20) NOT NULL,
  `memberid` bigint(20) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `comment`
--

INSERT INTO `comment` (`id`, `globalid`, `memberid`, `time`, `content`) VALUES
(1, 70, 42, '2014-12-18 19:34:46', 'Xin chào'),
(2, 70, 42, '2014-12-18 19:43:00', 'I want to tell you the most thing');

-- --------------------------------------------------------

--
-- Structure de la table `equipment`
--

CREATE TABLE IF NOT EXISTS `equipment` (
`id` bigint(20) NOT NULL,
  `name` text NOT NULL,
  `status` int(11) DEFAULT '-1',
  `model` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `manufacturer` text NOT NULL,
  `type` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `equipment`
--

INSERT INTO `equipment` (`id`, `name`, `status`, `model`, `quantity`, `manufacturer`, `type`, `description`) VALUES
(1, 'Dell Vostro 1450', -1, 'Dell Vostro 1450', 10, 'Dell', 'Laptop', 'Laptop này đã sữ dụng 1 năm.\r\nCấu hình: '),
(2, 'Oppo Joy R1001', -1, 'Oppo Joy R1001', 2, 'Oppo', 'Smart Phone', 'OPPO Joy R1001 mang trên mình thiết kế nhỏ gọn, vừa vặn trong lòng bàn tay người dùng. Mặt sau máy được làm từ nhựa cao cấp trong khi viền kim loại ở cạnh bên giả kim loại sáng bóng đầy sang trọng. Logo OPPO là điểm nhấn duy nhất ở mặt sau, nằm dưới camera và đèn flash. Cảm giác cầm máy rất thoải mái, cho phép bạn thao tác một thời gian dài mà không thấy mỏi mệt.');

-- --------------------------------------------------------

--
-- Structure de la table `global`
--

CREATE TABLE IF NOT EXISTS `global` (
`id` bigint(20) NOT NULL,
  `belongto` bigint(20) NOT NULL,
  `leaderid` bigint(20) NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `enddate` text NOT NULL,
  `name` text CHARACTER SET utf8 NOT NULL,
  `type` text NOT NULL,
  `startdate` text NOT NULL,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `global`
--

INSERT INTO `global` (`id`, `belongto`, `leaderid`, `description`, `enddate`, `name`, `type`, `startdate`, `createddate`) VALUES
(70, 0, 42, 'Xây dựng website "Quản lý dự án phần mềm"', '2014-12-22', 'Thực tập tốt nghiệp', 'PROJECT', '2014-09-09', '2014-12-16 04:02:23'),
(71, 70, 42, '', '2014-10-09', 'Đọc và hiểu đề tài', 'TASK', '2014-09-09', '2014-12-16 04:03:12'),
(72, 0, 43, 'Làm một website', '2014-12-12', 'Xây dựng phần mềm Quản lý dự án', 'PROJECT', '2014-09-09', '2014-12-16 04:12:51'),
(73, 72, 43, '', '2014-09-10', 'Đọc đề tài', 'TASK', '2014-09-09', '2014-12-16 04:21:54'),
(74, 73, 43, '', '2014-10-10', 'Tìm kiếm tài liệu', 'TASK', '2014-10-09', '2014-12-16 04:26:18'),
(75, 70, 43, '', '2014-12-22', 'Phân tích yêu cầu', 'TASK', '2014-11-07', '2014-12-16 04:35:26'),
(76, 71, 42, '', '2014-10-09', 'Đọc tài liệu', 'TASK', '2014-10-09', '2014-12-17 19:36:09');

-- --------------------------------------------------------

--
-- Structure de la table `global_equipment`
--

CREATE TABLE IF NOT EXISTS `global_equipment` (
`id` bigint(20) NOT NULL,
  `equipmentid` bigint(20) NOT NULL,
  `globalid` bigint(20) NOT NULL,
  `startdate` text NOT NULL,
  `enddate` text,
  `quality` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `global_member`
--

CREATE TABLE IF NOT EXISTS `global_member` (
`id` bigint(20) NOT NULL,
  `joineddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `memberid` bigint(20) DEFAULT NULL,
  `globalid` bigint(20) DEFAULT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8439 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `global_member`
--

INSERT INTO `global_member` (`id`, `joineddate`, `memberid`, `globalid`, `status`) VALUES
(8327, '2014-12-11 18:05:35', 41, 47, ''),
(8328, '2014-12-11 18:10:53', 0, 48, ''),
(8329, '2014-12-11 18:12:53', 0, 49, ''),
(8330, '2014-12-11 18:15:45', 0, 50, ''),
(8331, '2014-12-11 18:16:22', 0, 51, ''),
(8334, '2014-12-11 18:27:07', 42, 53, ''),
(8335, '2014-12-11 18:27:38', 42, 54, ''),
(8336, '2014-12-11 18:27:39', 41, 54, ''),
(8337, '2014-12-11 18:28:16', 42, 55, ''),
(8338, '2014-12-11 18:28:16', 41, 55, ''),
(8340, '2014-12-12 11:55:37', 42, 49, ''),
(8341, '2014-12-12 11:55:37', 41, 49, ''),
(8342, '2014-12-12 11:58:40', 0, 50, ''),
(8343, '2014-12-12 11:59:51', 0, 51, ''),
(8345, '2014-12-12 12:15:13', 0, 53, ''),
(8346, '2014-12-12 12:31:29', 0, 54, ''),
(8347, '2014-12-12 13:01:36', 0, 55, ''),
(8349, '2014-12-12 13:51:32', 0, 57, ''),
(8350, '2014-12-12 18:19:50', 0, 58, ''),
(8351, '2014-12-12 18:23:12', 0, 59, ''),
(8352, '2014-12-15 12:34:35', 0, 60, ''),
(8353, '2014-12-15 12:37:33', 0, 61, ''),
(8354, '2014-12-15 12:37:54', 0, 62, ''),
(8355, '2014-12-15 13:13:23', 42, 0, ''),
(8405, '2014-12-15 19:54:38', 0, 63, ''),
(8408, '2014-12-16 02:21:30', 42, 44, ''),
(8409, '2014-12-16 02:21:38', 0, 56, ''),
(8410, '2014-12-16 03:29:15', 41, 52, ''),
(8411, '2014-12-16 03:41:08', 0, 65, ''),
(8412, '2014-12-16 03:41:24', 0, 66, ''),
(8413, '2014-12-16 03:49:57', 0, 67, ''),
(8414, '2014-12-16 03:59:27', 0, 68, ''),
(8415, '2014-12-16 04:00:07', 0, 69, ''),
(8418, '2014-12-16 04:21:54', 42, 73, ''),
(8419, '2014-12-16 04:21:54', 43, 73, ''),
(8421, '2014-12-16 04:34:23', 0, 74, ''),
(8434, '2014-12-18 18:56:05', 42, 71, ''),
(8437, '2014-12-18 19:06:14', 0, 75, ''),
(8438, '2014-12-18 19:51:34', 0, 76, '');

-- --------------------------------------------------------

--
-- Structure de la table `member_skill`
--

CREATE TABLE IF NOT EXISTS `member_skill` (
`id` bigint(20) NOT NULL,
  `memberid` bigint(20) NOT NULL,
  `skillid` bigint(20) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `predecessor`
--

CREATE TABLE IF NOT EXISTS `predecessor` (
`id` bigint(20) NOT NULL,
  `child` bigint(20) NOT NULL,
  `parent` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `skill`
--

CREATE TABLE IF NOT EXISTS `skill` (
`id` bigint(20) NOT NULL,
  `name` text NOT NULL,
  `belongto` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `task`
--

CREATE TABLE IF NOT EXISTS `task` (
`id` bigint(20) NOT NULL,
  `belongto` bigint(20) NOT NULL,
  `leaderid` bigint(20) NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `enddate` text NOT NULL,
  `name` text CHARACTER SET utf8 NOT NULL,
  `type` text NOT NULL,
  `startdate` text NOT NULL,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `task`
--

INSERT INTO `task` (`id`, `belongto`, `leaderid`, `description`, `enddate`, `name`, `type`, `startdate`, `createddate`) VALUES
(70, 0, 42, 'Xây dựng website "Quản lý dự án phần mềm"', '2014-12-22', 'Thực tập tốt nghiệp', 'PROJECT', '2014-09-09', '2014-12-16 04:02:23'),
(71, 70, 42, '', '2014-10-09', 'Đọc và hiểu đề tài', 'TASK', '2014-09-09', '2014-12-16 04:03:12'),
(72, 0, 43, 'Làm một website', '2014-12-12', 'Xây dựng phần mềm Quản lý dự án', 'PROJECT', '2014-09-09', '2014-12-16 04:12:51'),
(73, 72, 43, '', '2014-09-10', 'Đọc đề tài', 'TASK', '2014-09-09', '2014-12-16 04:21:54'),
(74, 73, 43, '', '2014-10-10', 'Tìm kiếm tài liệu', 'TASK', '2014-10-09', '2014-12-16 04:26:18'),
(75, 70, 43, '', '2014-12-22', 'Phân tích yêu cầu', 'TASK', '2014-11-07', '2014-12-16 04:35:26'),
(76, 71, 42, '', '2014-10-09', 'Đọc tài liệu', 'TASK', '2014-10-09', '2014-12-17 19:36:09'),
(77, 72, 43, '', '2014-09-10', 'Đọc đề tài', 'TASK', '2014-09-09', '2014-12-16 04:21:54'),
(78, 0, 0, 'dgjdlsgfj;sd;', '3221-12-12', 'dsfdklshg', 'PROJECT', '2015-11-11', '2015-04-02 09:45:23'),
(79, 0, 0, 'ertyeryt', '', 'rtyhrety', 'PROJECT', '', '2015-04-02 09:51:15'),
(80, 78, 0, 'wrtyertytt', '', 'dfsygdfsy', 'TASK', '', '2015-04-02 09:51:43'),
(81, 76, 0, 'kdfgndfl', '0131-03-12', 'heldsf', 'TASK', '0013-03-12', '2015-04-02 09:52:42');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `account`
--
ALTER TABLE `account`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `equipment`
--
ALTER TABLE `equipment`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `global`
--
ALTER TABLE `global`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `global_equipment`
--
ALTER TABLE `global_equipment`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `global_member`
--
ALTER TABLE `global_member`
 ADD PRIMARY KEY (`id`), ADD KEY `globalid` (`globalid`);

--
-- Index pour la table `member_skill`
--
ALTER TABLE `member_skill`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `predecessor`
--
ALTER TABLE `predecessor`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `skill`
--
ALTER TABLE `skill`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `task`
--
ALTER TABLE `task`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `account`
--
ALTER TABLE `account`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `equipment`
--
ALTER TABLE `equipment`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `global`
--
ALTER TABLE `global`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT pour la table `global_equipment`
--
ALTER TABLE `global_equipment`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `global_member`
--
ALTER TABLE `global_member`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8439;
--
-- AUTO_INCREMENT pour la table `member_skill`
--
ALTER TABLE `member_skill`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `predecessor`
--
ALTER TABLE `predecessor`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `skill`
--
ALTER TABLE `skill`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `task`
--
ALTER TABLE `task`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=82;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
