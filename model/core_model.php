<?php
	class Core{
		public function __construct($object = null){
			if($object){
				$varList = get_class_vars($this->getTableName());
				foreach ($varList as $var => $value) {
					if(isset($object[$var]))
						$this->set($var, $object[$var]);
				}
			}
		}

		public function save(){
			if($this->id){
//				mysql_query("UPDATE `global` SET name = '$this->name',description = '$this->description',leaderid = '$this->leaderid',startdate = '$this->startdate',enddate = '$this->enddate',type = '$this->type',belongto = '$this->belongto' WHERE id = '$this->id'");
			}else{
				$this->set("id", "");
				$varList = get_class_vars($this->getTableName());
				$sqlQuery1 = "";
				$sqlQuery2 = "";

				$flag = false;
				foreach ($varList as $var => $value) {
					if(!$flag){
						$sqlQuery1.=$var;
						$sqlQuery2.="'".$this->get($var)."'";
					}else{
						$sqlQuery1.=",".$var;
						$sqlQuery2.=",'".$this->get($var)."'";
					}
					$flag = true;
				}
				if(!mysql_query("INSERT INTO ".$this->getTableName()."(".$sqlQuery1.") VALUES(".$sqlQuery2.")")) echo mysql_error();
			}			
		}

		public function remove(){
			if(in_array("belongto", get_class_vars(get_called_class()))){
				mysql_query("DELETE FROM ".$this->getTableName()." WHERE id = '".$this->id."'");
				$children = $this->getChildren();
				foreach ($children as $key => $child) {
					$child->remove();
				}
			}else{
				mysql_query("DELETE FROM ".$this->getTableName()." WHERE id = '".$this->id."'");
			}
		}

		public function getChildren(){
			$objArray = array();
			$tableName = $this->getTableName();
			$className = $this->getClassName();

			$objs = mysql_query("SELECT * FROM ".$tableName." WHERE belongto = '".$this->get("id")."'");

			while($obj = mysql_fetch_array($objs)){
				array_push($objArray, new $className($obj));
			}
			
			return $objArray;						
		}

		public function getParent(){
			$tableName = getTableName();

			$objs = mysql_query("SELECT * FROM ".$tableName." WHERE id = '".$this->get("belongto")."'");

			while($obj = mysql_fetch_array($objs)){
				return new $className($obj);
			}
			
			return null;						
		}

		public function find($args =  null){
			$objArray = array();
			$className = get_called_class();
			$tableName = strtolower($className);

			$flag = false;
			$sql = "";

			if($args){
				foreach ($args as $key => $value) {
					if(!$flag)
						$sql.=" WHERE ".$key."=".$args[$key];
					else
						$sql.=", ".$key."='".$args[$key]."'";

					$falg = true;
				}
			}
			
			$objs = mysql_query("SELECT * FROM ".$tableName.$sql);
			while($obj = mysql_fetch_array($objs)){
				array_push($objArray, new $className($obj));
			}
			
			return $objArray;			
		}

		public function set($variable, $value){
			$this->$variable = $value;
		}

		public function get($variable){
			return $this->$variable;
		}

		public function getNextID(){
			return $globalid = mysql_fetch_array(mysql_query("SELECT `auto_increment` as id FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'strtolower(get_class($this))'"))["id"];
		}

		public function getTableName(){
			return strtolower(get_called_class());
		}

		public function getClassName(){
			return get_called_class();
		}
	}
?>